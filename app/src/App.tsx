import './App.css';
import { useState, CSSProperties } from 'react';
import { QueryClient, QueryClientProvider, useQuery } from 'react-query';
// import { ReactQueryDevtools } from "react-query-devtools";
import InputPhone from './components/InputPhone';
const queryClient: QueryClient = new QueryClient();
const appCss: CSSProperties = {
  background: '#ababab',
  height: '300px',
};

function App() {
  const [modalOpened, setModalOpened] = useState<boolean>(true);
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App" style={appCss}>
        <InputPhone open={modalOpened} header="Change phone number" subheader="Provide new phone number" />
        <button
          onClick={() => {
            setModalOpened(true);
          }}
        >
          open modal
        </button>
      </div>
    </QueryClientProvider>
  );
}

export default App;
