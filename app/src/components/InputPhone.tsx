import { FC, CSSProperties, useState, KeyboardEvent } from 'react';
import { useQuery } from 'react-query';

interface Params  {
    open: boolean,
    header?:string,
    subheader?: string
};

interface Response {
  isLoading: boolean,
  data: object,
  error: object
};

interface Country {
    name: string,
    region: string,
    timezones: object,
    iso: object,
    phone: string[],
    emoji: string,
    image: string 
};


const dialogCss: CSSProperties = {
  borderRadius: '12px',
  border: '0px',
  width: '564px',
  height: '202px',
  padding: '24px',
};

const contentCss: CSSProperties = {
  width: '516px',
  height: '98px',
  gap: '34px',
};
const headerCss: CSSProperties = {
  fontSize: '18px',
  lineHeight: '24px',
  textAlign: 'left',
  width: '336px',
  height: '24px',
};

const selectCss: CSSProperties = {
    listStyleType: 'none',
    minHeight: '20px',
    minWidth: '50px',
    outline: '1px solid #000000',
    margin: '0px',
    padding: '0px',
    position: 'absolute'

};
const countryCss: CSSProperties = {
    display: 'flex',
    width: '120px',
    padding: '5px',
    background: 'rgba(255,255,255,.7)'
};

const labelCss: CSSProperties = {
  width: '172px',
  height: '16px',
  fontSize: '12px',
  top: '-18px',
  position: 'relative',
  gap: '2px',
  textAlign: 'left',
  display: 'flex',
  flexDirection: 'column',
};

const inputWrapperCss: CSSProperties = {
  display: 'flex',
  flexDirection: 'row',
  height: '20px'
};

const inputCss: CSSProperties = {
  position: 'relative',
  left: '140px'
}

const InputPhone:FC<Params> = ({ open, header, subheader }:Params) => {
    const [prefix, setPrefix] = useState('');
    let resp:any;
    
    resp = useQuery('repoData', () =>
    fetch(`http://localhost:8080/countries/prefix/${prefix || 'null'}`).then((res) => res.json())
  );
  
  if (resp.isLoading) return (<div>'Loading...'</div>);

  if (resp.error) return (<div>An error has occurred</div>);
  
    const changePrefix = async ($event:KeyboardEvent<HTMLElement>)=>{

        const { key } = $event;
        if(key==='Backspace') {
          await setPrefix('');
        };
        if(key.match(/^[A-Za-z]$/)) {
          await setPrefix(prefix+key);
        };
        resp.refetch();
    }
    const focusSelect = ()=>{
        document.getElementById('select')?.focus();
        console.log(document.activeElement)
    }
    const blurSelect = ()=>{
        document.getElementById('select')?.blur();
    }
    
    const handleInput = ($event:KeyboardEvent<HTMLInputElement>) =>{
      const { currentTarget, key } = $event;
      console.dir(currentTarget);
      if(key.match(/^[A-Za-z]$/)) {
        $event.preventDefault()
        changePrefix($event);
        return;
      }
      if(currentTarget.value.match(/[0-9]{3}-[0-9]{3}-[0-9]{3}/) && key.match(/^[0-9]$/)){
        $event.preventDefault();
        return;
      };

      if(currentTarget.value.match(/[0-9]{3}$/)) {
        if(key.match(/[0-9]/)) {
          currentTarget.value = (currentTarget.value+ '-' + key).slice(0,-1);
        }
        
      }; 

      
    }
    const handleClickCountry = (country:string)=>{
      if(Object.keys(resp.data).length===1) {
        setPrefix(prefix[0]);
      } else {
        setPrefix(country);
      }

      resp.refetch();
    }
  
  return (
      <div style={dialogCss}>
        <p style={headerCss}>{header}</p>
        <label style={labelCss}>
          { subheader }
          <div style={inputWrapperCss}>
            <ul id="select" style={selectCss} tabIndex={0} onMouseOver={focusSelect} onMouseLeave={blurSelect} onKeyDown={($event)=>{ changePrefix($event) }}>
            {
                Object.keys(resp.data).map(el=>{
                    return (
                    <li key={el} onClick={()=>{ handleClickCountry(resp.data[el].name) }} style={countryCss}>
                        <img height={20} width={'auto'} src={resp.data[el].image} />{resp.data[el].name} {resp.data[el].phone[0]}
                    </li>)
                })
            }
            </ul>
            <input onKeyDown={($event)=>{handleInput($event)}} type="tel" pattern="([0-9]{3}-){2}[0-9]" style={inputCss} />
          </div>
        </label>
      </div>
  );

}
export default InputPhone;