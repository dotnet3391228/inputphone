import express, { Express, Request, Response } from 'express';
const cors = require('cors')
const fs = require('fs')
const app = express()
const port = 8080


interface Country {
  [countryShort:string]: {
    name: string,
    region: string,
    timezones: object,
    iso: object,
    phone: string[],
    emoji: string,
    image: string
    }
};

const countries = JSON.parse(fs.readFileSync(__dirname+'/data/countries.json').toString());

app.use(cors());

app.get('/', (req:Request, res:Response) => {
  res.send('Hello World!')
})

app.get('/countries',(req:Request,res:Response)=>{
    res.json(countries);
})

app.get('/countries/prefix/:prefix',(req:Request,res: Response)=>{
  console.log(req.params)
  if(req.params.prefix==='null') {
    res.json({});
    return;
  };
  
  let filteredCountries = Object.keys(countries).filter(el=>
    countries[el].name.toLowerCase().startsWith(req.params.prefix.toLowerCase())
  );
  
  res.json(
    filteredCountries.reduce((obj, key) => {
      return Object.assign(obj, {
        [key]: countries[key]
      });
  }, {})  
  )
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})